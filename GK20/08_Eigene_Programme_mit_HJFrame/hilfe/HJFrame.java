package hilfe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public abstract class HJFrame extends JFrame implements ActionListener {
	// globale Variablen
	protected JPanel zeichenflaeche;

	public HJFrame(int width, int height, Color background, Color foreground, final String title) {
		super(title);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		zeichenflaeche = new JPanel() {
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				myPaint(g);
			}
		};
		zeichenflaeche.setPreferredSize(new Dimension(width, height));
		zeichenflaeche.setMaximumSize(new Dimension(width, height));
		zeichenflaeche.setMinimumSize(new Dimension(width, height));
		zeichenflaeche.setOpaque(true);
		zeichenflaeche.setDoubleBuffered(true);
		zeichenflaeche.setBackground(background);
		zeichenflaeche.setForeground(foreground);
		zeichenflaeche.setFont(new Font("Arial", Font.PLAIN, 12));
		contentPane.add(zeichenflaeche);
		setResizable(false);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		repaint();
	}
	
	abstract public void myPaint(Graphics g);
}