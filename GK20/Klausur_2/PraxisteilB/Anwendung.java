package KlausurNr2;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import hilfe.*;
import javax.swing.Timer;

public class Anwendung extends HJFrame {
	// globale Variablen
	private static final int WIDTH = 500;
	private static final int HEIGHT = 500;
	private static final Color BACKGROUND = Color.WHITE;
	private static final Color FOREGROUND = Color.BLACK;
	Hubschrauber Hubi1;
	Hubschrauber Hubi2;
	Hubschrauber Hubi3;

	public Anwendung(final String title) {
		super(WIDTH, HEIGHT, BACKGROUND, FOREGROUND, title);
		// eigene Initialisierung
		Hubi1 = new Hubschrauber(0,0,Color.GREEN);
		Hubi2 = new Hubschrauber(100,0,Color.CYAN);
		Hubi3 = new Hubschrauber(200,0,Color.RED);
		Timer timer = new Timer(100,this);
		timer.start();
	}

	@Override
	public void myPaint(Graphics g) {
		// wird aufgerufen, wenn das Fenster neu gezeichnet wird
		Hubi1.zeichnen(g);
		Hubi2.zeichnen(g);
		Hubi3.zeichnen(g);
	}

	public static void main(final String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Anwendung anwendung = new Anwendung("Anwendung");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}