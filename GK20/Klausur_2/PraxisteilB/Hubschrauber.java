package KlausurNr2;

import java.awt.*;


public class Hubschrauber {
	
	public int xPos;
	public int yPos;
	public Color Farbe;
	public int geschwindigkeit;
	
	public Hubschrauber(int xPos, int yPos, Color Farbe){
		this.xPos = xPos;
		this.yPos = yPos;
		this.Farbe = Farbe;
	}
	
	public void zeichnen(Graphics g){
		yPos = geschwindigkeit%500;
		xPos = geschwindigkeit%500;
		geschwindigkeit++;
		if(geschwindigkeit%2==0){
		g.setColor(Farbe);
		g.fillRect(xPos+0, yPos+20, 60, 10);
		g.fillOval(xPos+30, yPos+10, 60, 30);
		g.setColor(Color.BLACK);
		g.drawLine(xPos+60,yPos+0,xPos+60,yPos+50);
		g.drawLine(xPos+35, yPos+25, xPos+85, yPos+25);
			}
		else{
			g.setColor(Farbe);
			g.fillRect(xPos+0, yPos+20, 60, 10);
			g.fillOval(xPos+30, yPos+10, 60, 30);
			g.setColor(Color.BLACK);
			g.drawLine(xPos+40,yPos+10,xPos+80,yPos+40);
			g.drawLine(xPos+40, yPos+40, xPos+80, yPos+10);
		}
	}
}
