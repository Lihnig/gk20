package KlausurNr3;

import java.awt.*;


public class Ufo {
	
	public int xPos;
	public int yPos;
	public Color Farbe1;
	public Color Farbe2;

	public int geschwindigkeit;
	
	public Ufo(int xPos, int yPos, Color Farbe1, Color Farbe2){
		this.xPos = xPos;
		this.yPos = yPos;
		this.Farbe1 = Farbe1;
		this.Farbe2 = Farbe2;
	
	}
	
	public void zeichnen(Graphics g){
		yPos = geschwindigkeit%500;
		xPos = geschwindigkeit%500;
		geschwindigkeit++;
		if(geschwindigkeit%2==0){
		g.setColor(Farbe1);
		g.fillOval(xPos+15, yPos+0, 15, 30);
		g.setColor(Color.GRAY);
		g.fillOval(xPos+0, yPos+15, 45, 15);
		
			}
		else{
			g.setColor(Farbe2);
			g.fillOval(xPos+15, yPos+0, 15, 30);
			g.setColor(Color.GRAY);
			g.fillOval(xPos+0, yPos+15, 45, 15);
			
		}
	}
}
