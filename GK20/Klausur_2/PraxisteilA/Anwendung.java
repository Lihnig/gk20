package KlausurNr3;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import hilfe.*;
import javax.swing.Timer;

public class Anwendung extends HJFrame {
	// globale Variablen
	private static final int WIDTH = 500;
	private static final int HEIGHT = 500;
	private static final Color BACKGROUND = Color.BLACK;
	private static final Color FOREGROUND = Color.BLACK;
	Ufo Hubi1;
	Ufo Hubi2;
	//Ufo Hubi3;

	public Anwendung(final String title) {
		super(WIDTH, HEIGHT, BACKGROUND, FOREGROUND, title);
		// eigene Initialisierung
		Hubi1 = new Ufo(0,0,Color.GREEN,Color.RED);
		Hubi2 = new Ufo(100,0,Color.CYAN,Color.RED);
		//Hubi3 = new Ufo(200,0,Color.RED,Color.BLUE);
		Timer timer = new Timer(10,this);
		timer.start();
	}

	@Override
	public void myPaint(Graphics g) {
		// wird aufgerufen, wenn das Fenster neu gezeichnet wird
		Hubi1.zeichnen(g);
		Hubi2.zeichnen(g);
		//Hubi3.zeichnen(g);
	}

	public static void main(final String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Anwendung anwendung = new Anwendung("Anwendung");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}